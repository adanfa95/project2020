package com.example.androidprojectm1gl.Utils;

import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class util {

    public static void setStatusColor(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

//    //Chargement des fragments
//    public static void loadFragment(Fragment fragment, FragmentManager fragmentManager){
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.main_container_wrapper, fragment);
//        transaction.commit();
//    }
//
//    public static String getNumberFloatFormat(Float val){
//
//        NumberFormat numberFormat = NumberFormat.getInstance(java.util.Locale.FRENCH);
//
//        String valFormat = numberFormat.format(val);
//
//        return valFormat;
//    }

}
