package com.example.androidprojectm1gl.api;


import com.example.androidprojectm1gl.api.interface_api.Iconnexion;

public class ApiUtils {
    private static final String TAG = "ApiUtils";


    private static final String BASE_URL = "http://bibiche.alwaysdata.net/wwww/Myphp";


    private ApiUtils() {
    }

    public static Iconnexion getAPIConnexion() {
        return RetrofitClient.getClientAuth(BASE_URL).create(Iconnexion.class);
    }


}

