package com.example.androidprojectm1gl.api.interface_api;

import com.example.androidprojectm1gl.models.ConnexionResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Iconnexion {

    @Headers("Content-Type: application/json")
    @POST("connexion.php")
    Call<ConnexionResponse> authenticate(@Query("identifiant") String identifiant, @Query("password") String password);
}
