package com.example.androidprojectm1gl.activites;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;

import com.example.androidprojectm1gl.R;
import com.example.androidprojectm1gl.databinding.ActivityLoginBinding;

import org.json.JSONObject;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;

    private EditText txtLogin, txtPassword;
    private TextView btnSignIn;
    private Button btnConnect;
    private String login, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),InscriptionActivity.class));
            }
        });


        binding.btnConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login = binding.etIdentifiant.getText().toString().trim();
                password = binding.etPassword.getText().toString();

                if(login.isEmpty() || password.isEmpty())
                {
                    String error = getString(R.string.error_fields);
                    Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    authentification(login, password);
                }
            }
        });

    }

    public void authentification(String login, String password) {
        try {
            String url = "http://192.168.1.14:81/projetAndroid/connexion.php?identifiant=" + login + "&password=" + password;
            OkHttpClient httpClient = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url).build();
            httpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String error = getString(R.string.error_connection);
                            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
                        }
                    });

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        String status = object.getString("status");

                        if (status.equalsIgnoreCase("KO")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String message = getString(R.string.error_connection);
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                        } else {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
