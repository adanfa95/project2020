package com.example.androidprojectm1gl.activites;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;

import com.example.androidprojectm1gl.R;
import com.example.androidprojectm1gl.databinding.ActivityConfirmInscriptionBinding;
import com.example.androidprojectm1gl.databinding.ActivityInscriptionBinding;

import org.json.JSONObject;

import java.io.IOException;

public class InscriptionActivity extends AppCompatActivity {
    private String nom, prenom, email;

    private ActivityInscriptionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityInscriptionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.precedent.bringToFront();
      binding.suivant.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              nom = binding.nom.getText().toString().trim();
              prenom = binding.prenom.getText().toString().trim();
              email = binding.email.getText().toString().trim();
              if (nom.isEmpty() || prenom.isEmpty() || email.isEmpty() ){

                  String error = getString(R.string.error_fields);
                  Toast.makeText(InscriptionActivity.this, error, Toast.LENGTH_SHORT).show();
              }
              else
              {
                  Intent intent = new Intent(getApplicationContext(),ConfirmInscriptionActivity.class);
                  intent.putExtra("nom",nom);
                  intent.putExtra("prenom",prenom);
                  intent.putExtra("email",email);
              startActivity(intent);
              }

          }
      });

        binding.precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}