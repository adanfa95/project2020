package com.example.androidprojectm1gl.activites;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.androidprojectm1gl.R;
import com.example.androidprojectm1gl.databinding.ActivityConfirmInscriptionBinding;
import com.example.androidprojectm1gl.databinding.ActivityInscriptionBinding;

import org.json.JSONObject;

import java.io.IOException;

public class ConfirmInscriptionActivity extends AppCompatActivity {
    private ActivityConfirmInscriptionBinding binding;
    private ActivityInscriptionBinding bindingFirst;

    private String nom, prenom, password, email,identifiant,confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityConfirmInscriptionBinding.inflate(getLayoutInflater());
        //bindingFirst=ActivityInscriptionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.precedent.bringToFront();
        binding.btnInscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nom= getIntent().getStringExtra("nom");
                prenom= getIntent().getStringExtra("prenom");
                email= getIntent().getStringExtra("email");
                identifiant = binding.etIdentifiant.getText().toString().trim();
                password = binding.etPassword.getText().toString();
                confirmPassword = binding.confirmMotPasse.getText().toString();

                if ( identifiant.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()){

                    String error = getString(R.string.error_fields);
                    Toast.makeText(ConfirmInscriptionActivity.this, error, Toast.LENGTH_SHORT).show();
                }
                else if(!password.equals(confirmPassword)){
                    String error = getString(R.string.error_password_confirm);
                    Toast.makeText(ConfirmInscriptionActivity.this, error, Toast.LENGTH_SHORT).show();

                }
                else
                {
                    inscription();
                }
            }
        });

    }

    public void inscription(){

        try {

            String url = "http://192.168.1.14:81/projetAndroid/inscription.php?nom=" + nom + "&prenom=" + prenom+ "&email=" + email + "&password=" + password + "&identifiant=" + identifiant;
            OkHttpClient client =new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url).build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String error = getString(R.string.error_connection);
                            Toast.makeText(ConfirmInscriptionActivity.this, error, Toast.LENGTH_SHORT).show();

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        String status = object.getString("status");

                        if (status.equalsIgnoreCase("KO")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String message = getString(R.string.error_parameters_inscription);
                                    Toast.makeText(ConfirmInscriptionActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                        } else {
                            Intent intent = new Intent(ConfirmInscriptionActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
