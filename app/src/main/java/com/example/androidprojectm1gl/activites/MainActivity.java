package com.example.androidprojectm1gl.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.example.androidprojectm1gl.fragment.AccueilFragment;
import com.example.androidprojectm1gl.fragment.EquipeFragment;
import com.example.androidprojectm1gl.fragment.ParametreFragment;
import com.example.androidprojectm1gl.R;
import com.example.androidprojectm1gl.Utils.util;
import com.example.androidprojectm1gl.adapter.ViewPagerAdapter;
import com.example.androidprojectm1gl.databinding.ActivityMainBinding;
import com.example.androidprojectm1gl.fragment.MatchFragment;

import cn.pedant.SweetAlert.SweetAlertDialog;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        util.setStatusColor(getWindow());
        setContentView(binding.getRoot());
        setupViewPager(binding.viewPager);


        binding.bottomNavigation.add(new MeowBottomNavigation.Model(0, R.drawable.ic_homeb));
        binding.bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_soccer));
        binding.bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_player));
        binding.bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_settings));
        binding.bottomNavigation.show(0, true);
        binding.bottomNavigation.setOnClickMenuListener(new Function1<MeowBottomNavigation.Model, Unit>() {
            @Override
            public Unit invoke(MeowBottomNavigation.Model model) {
                binding.viewPager.setCurrentItem(model.getId());
                return null;
            }
        });

        binding.viewPager.setOffscreenPageLimit(3);

        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                binding.bottomNavigation.show(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    ViewPagerAdapter adapter;
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AccueilFragment(), getString(R.string.accueil));
        adapter.addFragment(new MatchFragment(), getString(R.string.match) );
        adapter.addFragment(new EquipeFragment(),getString(R.string.equipe) );
        adapter.addFragment(new ParametreFragment(),getString(R.string.parametre) );
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        MainActivity.this.runOnUiThread(() -> {
            final SweetAlertDialog qDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE);
            qDialog.setTitleText("Voulez vous quitter application? ");
            qDialog.setCancelable(true);
            qDialog.setCancelText(getString(R.string.annuler));
            qDialog.setConfirmButton(getString(R.string.quitter), sDialog -> {
                finishAffinity();
                System.exit(0);
            });
            qDialog.show();
            //qDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM).setBackground(getColor(R.color.colorPrimary));
        });

    }


}
