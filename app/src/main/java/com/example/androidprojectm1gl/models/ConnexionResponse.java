package com.example.androidprojectm1gl.models;

import com.google.gson.annotations.SerializedName;

public class ConnexionResponse{

    @SerializedName("status")
    private String status;

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }
}